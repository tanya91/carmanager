/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.starr.carmanager.dao;

import com.starr.carmanager.entity.Car;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Tanya
 */
@Repository
public class CarDAOImpl implements CarDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addCar(Car car) {
        Session session = sessionFactory.getCurrentSession();
        Transaction trans = session.beginTransaction();
        session.save(car);
        trans.commit();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Car> listCar() {

        return sessionFactory.getCurrentSession().createQuery("from Car")
                .list();
    }

    @Override
    public void removeCar(Car car) {
        {
            Session session = sessionFactory.getCurrentSession();
            Transaction trans = session.beginTransaction();
            session.delete(car);
            trans.commit();
        }
    }

    @Override
    public Car getCar(Integer id) {
        return (Car) sessionFactory.getCurrentSession().get(Car.class, id);
    }

}
