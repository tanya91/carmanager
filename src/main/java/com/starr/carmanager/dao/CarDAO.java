/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.starr.carmanager.dao;

import com.starr.carmanager.entity.Car;
import java.util.List;

/**
 *
 * @author Tanya
 */
public interface CarDAO {
    public void addCar(Car car);

    public List<Car> listCar();

    public void removeCar(Car car);
    
    public Car getCar(Integer id);
}
