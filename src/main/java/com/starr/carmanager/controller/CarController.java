/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.starr.carmanager.controller;

import com.starr.carmanager.entity.Car;
import com.starr.carmanager.service.CarService;
import com.starr.carmanager.service.CarServiceImpl;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Tanya
 */
public class CarController {

    @Autowired
    private CarService carService;

    @RequestMapping(method = {RequestMethod.GET})
    public String getPage(Model model) {
        model.addAttribute("car", new Car());
        return "index";
    }

    @RequestMapping(method = {RequestMethod.POST})
    public String addCar(@ModelAttribute("car") Car car,
            Model model, HttpServletRequest req) {
        carService.addCar(car);
        model.addAttribute("done", "done:"+car.getPrice()+" "+car.getYear()+" "+car.getColor()+" "+car.getModel()+" "+car.getId());
        return "index";
    }

}
