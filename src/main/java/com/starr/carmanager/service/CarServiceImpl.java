/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.starr.carmanager.service;

import com.starr.carmanager.dao.CarDAO;
import com.starr.carmanager.dao.CarDAOImpl;
import com.starr.carmanager.entity.Car;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Tanya
 */
@Service
public class CarServiceImpl implements CarService {

   @Autowired
    private CarDAO carDAO;

    @Transactional
    @Override
    public void addCar(Car car) {
        carDAO.addCar(car);
    }

    @Transactional
    @Override
    public List<Car> listCar() {
        try {
            return carDAO.listCar();
        } catch (NullPointerException ex) {
            return null;
        }
    }

    @Transactional
    @Override
    public void removeCar(Car car) {
        carDAO.removeCar(car);
    }

    /**
     *
     * @param id
     * @return
     */
    @Transactional
    @Override
    public Car getCar(Integer id) {
        return carDAO.getCar(id);
    }

}


