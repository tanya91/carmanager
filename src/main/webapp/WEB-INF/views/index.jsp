<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Welcome to Spring Web MVC project</title>
    </head>

    <body>
    <h1>Car Manager</h1>
            <form:form method="POST" modelAttribute="car">
                <table>
                    <tr>
                        <td> Model:</td> <td><form:input path="model" type="text"/> </td>
                    </tr>
                    <tr>
                        <td> Year:</td> <td><form:input path="year" type="text"/> </td>
                    </tr>
                    <tr>
                        <td> Color:</td> <td><form:input path="color" type="color"/> </td>
                    </tr>
                    <tr>
                        <td> Price:</td> <td><form:input path="price" type="text"/> </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td align='right'><input type="submit" value="Add"/></td>
                    </tr>
                </table>
           </form:form>
    ${done}
    </body>
</html>
